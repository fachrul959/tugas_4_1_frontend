import * as React from "react";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "bootstrap/dist/css/bootstrap.css";

import { BrowserRouter as Router , Routes, Route, Link } from "react-router-dom";

import { Nav } from "react-bootstrap";
import EditProvinsi from "./components/provinsi/edit.component";
import ProvinsiList from "./components/provinsi/list.component";
import CreateProvinsi from "./components/provinsi/create.component";
import KabupatenList from "./components/kabupaten/list.component";
import EditKabupaten from "./components/kabupaten/edit.component";
import CreateKabupaten from "./components/kabupaten/create.component";
import CreateKecamatan from "./components/kecamatan/create.component";
import EditKecamatan from "./components/kecamatan/edit.component";
import KecamatanList from "./components/kecamatan/list.component";
import CreateKelurahan from "./components/kelurahan/create.component";
import EditKelurahan from "./components/kelurahan/edit.component";
import KelurahanList from "./components/kelurahan/list.component";

function App() {
  return (<Router>
    <Navbar bg="light" expand="lg">
      <Container>
        <Link to={"/"} className="navbar-brand">
          CRUD Daerah
        </Link>
        <Nav className="me-auto">
        <Link to={"/"} className="nav-link">
          Provinsi
        </Link>
        <Link to={"/kabupaten"} className="nav-link">
          Kabupaten
        </Link>
        <Link to={"/kecamatan"} className="nav-link">
          Kecamatan
        </Link>
        <Link to={"/kelurahan"} className="nav-link">
          Kelurahan
          </Link>
          </Nav>
      </Container>
    </Navbar>

    <Container className="mt-5">
      <Row>
        <Col md={12}>
          <Routes>
            <Route path="/provinsi/create" element={<CreateProvinsi />} />
            <Route path="/provinsi/edit/:id" element={<EditProvinsi />} />
            <Route exact path='/' element={<ProvinsiList />} />
            <Route path="/kabupaten/create" element={<CreateKabupaten />} />
            <Route path="/kabupaten/edit/:id" element={<EditKabupaten />} />
            <Route exact path='/kabupaten' element={<KabupatenList />} />
            <Route path="/kecamatan/create" element={<CreateKecamatan />} />
            <Route path="/kecamatan/edit/:id" element={<EditKecamatan />} />
            <Route exact path='/kecamatan' element={<KecamatanList />} />
            <Route path="/kelurahan/create" element={<CreateKelurahan />} />
            <Route path="/kelurahan/edit/:id" element={<EditKelurahan />} />
            <Route exact path='/kelurahan' element={<KelurahanList />} />
          </Routes>
        </Col>
      </Row>
    </Container>
  </Router>);
}

export default App;
