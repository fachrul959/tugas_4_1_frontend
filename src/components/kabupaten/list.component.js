import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button'
import axios from 'axios';
import Swal from 'sweetalert2'

export default function KabupatenList() {

    const [kabupaten, setKabupaten] = useState([])

    useEffect(()=>{
        fetch() 
    },[])

    const fetch = async () => {
        await axios.get(`http://crud-daerah.test/api/kabupaten`).then(({data})=>{
            setKabupaten(data)
        })
    }

    const deleteData = async (id) => {
        const isConfirm = await Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            return result.isConfirmed
          });

          if(!isConfirm){
            return;
          }

          await axios.delete(`http://crud-daerah.test/api/kabupaten/${id}`).then(({data})=>{
            Swal.fire({
                icon:"success",
                text:data.message
            })
            fetch()
          }).catch(({response:{data}})=>{
            Swal.fire({
                text:data.message,
                icon:"error"
            })
          })
    }

    return (
      <div className="container">
          <div className="row">
            <div className='col-12'>
                <Link className='btn btn-primary mb-2 float-end' to={"/kabupaten/create"}>
                    Create Kabupaten
                </Link>
            </div>
            <div className="col-12">
                <div className="card card-body">
                    <div className="table-responsive">
                        <table className="table table-bordered mb-0 text-center">
                            <thead>
                                <tr>
                                    <th>ID_Kab</th>
                                    <th>ID_Prov</th>
                                    <th>Nama</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    kabupaten.length > 0 && (
                                        kabupaten.map((row, key)=>(
                                            <tr key={key}>
                                                <td>{row.id_kab}</td>
                                                <td>{row.id_prov}</td>
                                                <td>{row.nama}</td>
                                                <td>
                                                    <Link to={`/kabupaten/edit/${row.id_kab}`} className='btn btn-success me-2'>
                                                        Edit
                                                    </Link>
                                                    <Button variant="danger" onClick={()=>deleteData(row.id_kab)}>
                                                        Delete
                                                    </Button>
                                                </td>
                                            </tr>
                                        ))
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
      </div>
    )
}