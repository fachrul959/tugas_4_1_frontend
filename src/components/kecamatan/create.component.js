import React, { useState } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import axios from 'axios'
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom'

export default function CreateKecamatan() {
  const navigate = useNavigate();

  const [idkab, setidkab] = useState("")
  const [idkec, setidkec] = useState("")
    const [nama, setnama] = useState("")
    const [validationError,setValidationError] = useState({})


  const createKecamatan = async (e) => {
    e.preventDefault();

    const formData = new FormData()

    formData.append('id_kec', idkec)
    formData.append('id_kab', idkab)
    formData.append('nama', nama)

    await axios.post(`http://crud-daerah.test/api/kecamatan`, formData).then(({data})=>{
      Swal.fire({
        icon:"success",
        text:data.message
      })
      navigate("/")
    }).catch(({response})=>{
      if(response.status===422){
        setValidationError(response.data.errors)
      }else{
        Swal.fire({
          text:response.data.message,
          icon:"error"
        })
      }
    })
  }

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-12 col-sm-12 col-md-6">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Create Kecamatan</h4>
              <hr />
              <div className="form-wrapper">
                {
                  Object.keys(validationError).length > 0 && (
                    <div className="row">
                      <div className="col-12">
                        <div className="alert alert-danger">
                          <ul className="mb-0">
                            {
                              Object.entries(validationError).map(([key, value])=>(
                                <li key={key}>{value}</li>   
                              ))
                            }
                          </ul>
                        </div>
                      </div>
                    </div>
                  )
                }
                <Form onSubmit={createKecamatan}>
                  <Row> 
                      <Col>
                        <Form.Group controlId="Name">
                            <Form.Label>id_kec</Form.Label>
                            <Form.Control type="number" value={idkec} onChange={(event)=>{
                              setidkec(event.target.value)
                            }}/>
                      </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group controlId="Name">
                            <Form.Label>Id_kab</Form.Label>
                            <Form.Control type="number" value={idkab} onChange={(event)=>{
                              setidkab(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                  </Row>
                  <Row className="my-3">
                      <Col>
                        <Form.Group controlId="Description">
                            <Form.Label>Nama</Form.Label>
                            <Form.Control type="text" value={nama} onChange={(event)=>{
                              setnama(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>
                  </Row>
                  <Button variant="primary" className="mt-2" size="lg" block="block" type="submit">
                    Save
                  </Button>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}