import React, { useEffect, useState } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useNavigate, useParams } from 'react-router-dom'
import axios from 'axios';
import Swal from 'sweetalert2';

export default function EditKelurahan() {
    const navigate = useNavigate();
    const { id } = useParams()

    const [idkel, setidkel] = useState("")
    const [idkec, setidkec] = useState("")
    const [nama, setnama] = useState("")
    const [validationError,setValidationError] = useState({})

  useEffect(()=>{
    fetch()
  },[])

  const fetch = async () => {
    await axios.get(`http://crud-daerah.test/api/kelurahan/${id}`).then(({data})=>{
      const { id_kel, id_kec, nama } = data.kelurahan
      setidkel(id_kel)
      setidkec(id_kec)
      setnama(nama)
    }).catch(({response:{data}})=>{
      Swal.fire({
        text:data.message,
        icon:"error"
      })
    })
  }


  const update = async (e) => {
    e.preventDefault();

    const formData = new FormData()
    formData.append('_method', 'PATCH');
    formData.append('id_kel', idkel)
    formData.append('id_kec', idkec)
    formData.append('nama', nama)

    await axios.post(`http://crud-daerah.test/api/kelurahan/${idkel}`, formData).then(({data})=>{
      Swal.fire({
        icon:"success",
        text:data.message
      })
      navigate("/")
    }).catch(({response})=>{
      if(response.status===422){
        setValidationError(response.data.errors)
      }else{
        Swal.fire({
          text:response.data.message,
          icon:"error"
        })
      }
    })
  }

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-12 col-sm-12 col-md-6">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Update Kelurahan</h4>
              <hr />
              <div className="form-wrapper">
                {
                  Object.keys(validationError).length > 0 && (
                    <div className="row">
                      <div className="col-12">
                        <div className="alert alert-danger">
                          <ul className="mb-0">
                            {
                              Object.entries(validationError).map(([key, value])=>(
                                <li key={key}>{value}</li>   
                              ))
                            }
                          </ul>
                        </div>
                      </div>
                    </div>
                  )
                }
                <Form onSubmit={update}>
                  <Row> 
                      <Col>
                        <Form.Group controlId="Name">
                            <Form.Label>ID_Kel</Form.Label>
                            <Form.Control type="number" value={idkel} onChange={(event)=>{
                              setidkel(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                      <Col>
                        <Form.Group controlId="Name">
                            <Form.Label>ID_Kec</Form.Label>
                            <Form.Control type="number" value={idkec} onChange={(event)=>{
                              setidkec(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>  
                  </Row>
                  <Row className="my-3">
                      <Col>
                        <Form.Group controlId="Description">
                            <Form.Label>Nama</Form.Label>
                            <Form.Control type="text" value={nama} onChange={(event)=>{
                              setnama(event.target.value)
                            }}/>
                        </Form.Group>
                      </Col>
                  </Row>
                  <Button variant="primary" className="mt-2" size="md" block="block" type="submit">
                    Update
                  </Button>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}