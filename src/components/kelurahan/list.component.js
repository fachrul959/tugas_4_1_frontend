import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button'
import axios from 'axios';
import Swal from 'sweetalert2'

export default function KelurahanList() {

    const [kelurahan, setKelurahan] = useState([])

    useEffect(()=>{
        fetch() 
    },[])

    const fetch = async () => {
        await axios.get(`http://crud-daerah.test/api/kelurahan`).then(({data})=>{
            setKelurahan(data)
        })
    }

    const deleteData = async (id) => {
        const isConfirm = await Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            return result.isConfirmed
          });

          if(!isConfirm){
            return;
          }

          await axios.delete(`http://crud-daerah.test/api/kelurahan/${id}`).then(({data})=>{
            Swal.fire({
                icon:"success",
                text:data.message
            })
            fetch()
          }).catch(({response:{data}})=>{
            Swal.fire({
                text:data.message,
                icon:"error"
            })
          })
    }

    return (
      <div className="container">
          <div className="row">
            <div className='col-12'>
                <Link className='btn btn-primary mb-2 float-end' to={"/kelurahan/create"}>
                    Create Kelurahan
                </Link>
            </div>
            <div className="col-12">
                <div className="card card-body">
                    <div className="table-responsive">
                        <table className="table table-bordered mb-0 text-center">
                            <thead>
                                <tr>
                                    <th>ID_Kel</th>
                                    <th>ID_Kec</th>
                                    <th>Nama</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    kelurahan.length > 0 && (
                                        kelurahan.map((row, key)=>(
                                            <tr key={key}>
                                                <td>{row.id_kel}</td>
                                                <td>{row.id_kec}</td>
                                                <td>{row.nama}</td>
                                                <td>
                                                    <Link to={`/kelurahan/edit/${row.id_kel}`} className='btn btn-success me-2'>
                                                        Edit
                                                    </Link>
                                                    <Button variant="danger" onClick={()=>deleteData(row.id_kel)}>
                                                        Delete
                                                    </Button>
                                                </td>
                                            </tr>
                                        ))
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
      </div>
    )
}